﻿using System;

namespace HelloYou
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello user!");
            Console.WriteLine("My name is Jonas Horvli.");
            Console.WriteLine("What is your name?");
            string userName = Console.ReadLine();
            int nameLength = userName.Length;
            Console.WriteLine($"Hello {userName}, you name is {userName.Length} characters long and starts with a {userName[0]}");
        }
    }
}
